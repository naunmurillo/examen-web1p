function validar() {
    code = document.getElementById("code").value;
    marca = document.getElementById("marca").value;
    modelo = document.getElementById("modelo").value;
    año = document.getElementById("año").value;
    fechai = document.getElementById("fechai").value;
    fechaf = document.getElementById("fechaf").value;
    if (code.length > 5) {
      alert("El codigo debe ser maximo 5 caracteres");
      document.getElementById("code").value = "";
      document.getElementById("code").focus();
      return false;
    }
    if (marca.length > 50) {
        alert("la marca debe ser maximo 50 caracteres");
        document.getElementById("marca").value = "";
        document.getElementById("marca").focus();
        return false;
      }
      if (modelo.length > 30) {
        alert("El modelo debe ser maximo 30 caracteres");
        document.getElementById("modelo").value = "";
        document.getElementById("modelo").focus();
        return false;
      }
      if (año.length > 4) {
        alert("El año debe ser maximo 4 caracteres");
        document.getElementById("año").value = "";
        document.getElementById("año").focus();
        return false;
      }
    if (fechaf < fechai) {
      alert("La fecha de final debe ser mayor a la fecha inicial.");
      document.getElementById("datei").focus();
      return false;
    }
  
    alert("datos enviados con exito");
  }
  
  
